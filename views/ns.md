### Getting started

You'll need to be signed up on this Summit instance and our [GitLab](https://gitlab.com/serpent-os) to begin contributing. Send your merge
requests to any of our packages and a maintainer will tag the build.

### Becoming a maintainer

Paths are open for contributors to become maintainers at a project level,
allowing direct involvement in the release and update process. Check out
the [packaging docs](#) to find out more.